#!/bin/bash
# ------------------------------------------------------------------
# Copyright:  Patrick Byrne
# License:    Apache 2.0
# Author:     Patrick Byrne
# Title:      check_hwg_xml      
# Description:
#             Parse HWg device XML for values and counters
# ------------------------------------------------------------------
#    Todo:
# ------------------------------------------------------------------

# --- Variables ----------------------------------------------------

version=1.0.0

# --- Functions ----------------------------------------------------

version()	
{
  printf "Version: %s\n" $version
}

usage() 
{
    cat <<"STOP"
    Usage: check_hwg_xml.sh -e 12
            or
           check_hwg_xml.sh -l

    OPTIONS

      -h | --help       Print this message
      -v | --version    Print Version 
      -H | --host       Host or IP to query
      -l | --list       List all sensor entries
      -e | --entry id   Select an entry to query
      -r | --rate       Enable rate calculation

STOP
}

# Splits XML tags
split_tags () {
    local IFS=\>
    read -r -d \< xml_key xml_value
}

format_message () {
  message="OK - ID:$id Name: $name"
  if [[ -n $rate_enabled ]]; then
    message="$message Rate: ${rate:-Calculating}"
  else
    message="$message Value: $value"
  fi
}

format_perfdata () {
  perfdata="value=$value;0;0;0;0"
  if [[ -n $counter ]]; then
    perfdata="$perfdata counter=$counter;0;0;0;0"
  fi
  if [[ -n $rate_enabled ]]; then
    perfdata="$perfdata rate=${rate:-0};0;0;0;0"
  fi
}

calculate_rate () {
  filename="/var/tmp/${host}_${id}_rate.state"
  if [[ -n $rate_enabled ]] && [[ -f $filename ]]; then
    read -r state_time state_counter < "$filename"
    current_time=$(date +%s)
    # Get the time delta in minutes
    delta_time=$(bc <<< "scale=2; (${current_time}-${state_time})/60")
    # Get the counter delta
    delta_counter=$(bc <<< "${counter}-${state_counter}")
    # Divide the counter delta by the time delta to get the rate
    rate=$(bc <<< "scale=2; ${delta_counter}/${delta_time}")
  fi
  if [[ -n $rate_enabled ]]; then
    # Set the variables in the state file
    echo "$(date +%s) $counter" > "$filename"
  fi
}


# --- Options processing -------------------------------------------

while [[ $# -gt 0 ]]; do
  param=$1
  option_value=$2
  case $param in
    -h | --help)
      usage
      exit
      ;;
    -v | --version)
      version
      exit
      ;;
    -H | --host)
      host=$option_value
      shift
      ;;
    -l | --list)
      mode="list"
      ;;
    -e | --entry)
      mode="query"
      entry=$option_value
      shift
      ;;
    -r | --rate)
      rate_enabled="true"
      ;;
    *)
      echo "Error: unknown parameter \"$param\""
      usage
      exit 1
      ;;
  esac
  shift
done

# Verify that the host variable is set
if [[ -z $host ]]; then
  echo "Error: No host"
  usage
  exit 1
fi

# Verify that the mode variable is set
if [[ -z $mode ]]; then
  echo "Error: No mode selected"
  usage
  exit 1
fi

# --- Body ---------------------------------------------------------

# Get data
page=$(curl -s -L "$host")

# Loop through the page and split the keys from the values
while split_tags; do
  # Check the key type and store the value or trigger an action
  case $xml_key in
    Name)
      name=$xml_value
    ;;
    ID)
      id=$xml_value
    ;;
    Value)
      value=$xml_value
    ;;
    Counter)
      counter=$xml_value
    ;;
    Entry)
      unset name
      unset id
      unset value
      unset counter
    ;;
    /Entry)
      # List mode - display all the valid entries that have values
      if [[ $mode = "list" ]] && [[ -n $value ]]; then
        echo "ID:$id Name: $name"
      # Query mode - Find the matching entry by id
      elif [[ $mode = "query" ]] && [[ $entry = "$id" ]]; then
        calculate_rate
        format_message
        format_perfdata
        echo "${message}|${perfdata}"
        exit 0
      fi
  esac
done <<< "$page"

