## check_hwg_xml

This is a simple bash script to parse values from HWg devices

#### Requirements

  * bc
  * curl

#### Usage

    Usage: check_hwg_xml.sh -e 12
            or
           check_hwg_xml.sh -l

    OPTIONS

      -h | --help       Print this message
      -v | --version    Print Version 
      -H | --host       Host or IP to query
      -l | --list       List all sensor entries
      -e | --entry id   Select an entry to query
      -r | --rate       Enable rate calculation

